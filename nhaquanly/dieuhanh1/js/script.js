﻿var lat = 10.752391;
var lon = 106.729860;

var map = new L.Map('map', {
    zoom: 20,
    minZoom: 4,
});



// create a new tile layer
var tileUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    layer = new L.TileLayer(tileUrl, {
        attribution: '', //'Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
        maxZoom: 16
    });

// add the layer to the map
map.addLayer(layer);

var cuuthuong = getParameterByName('ct');
var cuuhoa = getParameterByName('ch');
var chihuy = getParameterByName('chh');

// var lstPolice = [{
//     name: 'Công an phường Đông Ngạc',
//     lat: '21.0863999',
//     lon: '105.78120060000003',
//     address: 'Ngõ 35 Đông Ngạc, Đông Ngạc, Bắc Từ Liêm, Hà Nội'
// },{
//     name: 'Công an phường Thụy Phương',
//     lat: '21.0925584',
//     lon: '105.77541229999997',
//     address: 'Thụy Phương, Thuỵ Phương, Từ Liêm, Hà Nội'
// },{
//     name: 'Công an phường Phú Thượng',
//     lat: '21.0883183',
//     lon: '105.80889520000005',
//     address:'379 Lạc Long Quân, Phú Thượng, Tây Hồ, Hà Nội'
// }];

/*var lstFirefighter = [{
    name: 'PCCC số 3 huyện Tân Thành',
    lat: '10.0426',
    lon: '105.7783',
    address: 'TT. Phú Mỹ, Tân Thành, Bà Rịa - Vũng Tàu, Việt Nam'
}]*/

var lstFirefighter = [{
    name: 'PCCC TP Hồ Chí Minh',
    lat: '10.760565',
    lon: '106.688507',
    address: 'Cảnh sát PCCC TP Hồ Chí Minh, 258 Đường Trần Hưng Đạo, Phường Nguyễn Cư Trinh, Quận 1, Hồ Chí Minh, Việt Nam'
}]

var lstHospital = [{
    name: 'Bệnh viện Nhi Đồng 2',
    lat: '10.78067',
    lon: '106.70357',
    address: 'Bệnh viện Nhi Đồng 2, 33 Nguyễn Du, Bến Nghé, Quận 1, Hồ Chí Minh, Việt Nam'
}]

var lstHospital2 = [{
    name: 'Bệnh viện Quận 7',
    lat: '10.73789',
    lon: '106.72405',
    address: 'Bệnh viện Quận 7, Nguyễn Thị Thập, Tân Phú, Quận 7, Hồ Chí Minh, Việt Nam'
}]


var xethang1 = [
    [lstFirefighter[0].lat, lstFirefighter[0].lon],
    [10.77084,106.69821],
    [10.77068,106.70630],
    [10.76851,106.70564],
    [10.76246,106.70832],
    [10.75191,106.72476],
    [lat, lon]
];
var xethang2 = [
    [lstFirefighter[0].lat, lstFirefighter[0].lon],
    [10.76158,106.68976],
    [10.75864,106.69248],
    [10.76921,106.70541],
    [10.76250,106.70833],
    [10.75439,106.72779],
    [10.75264,106.72851],
    [lat, lon]
];

/*
var xecuuthuong1 = [
    [lstHospital[0].lat, lstHospital[0].lon],
    [10.59720,107.05835],
    [10.59394,107.05825],
    [10.59402,107.05460],
    [10.58932,107.05517],
    [10.58824,107.05512],
    [0.58877,107.04084],
    [lat, lon]
];
*/
/*
var xecuuthuong1 = [
    [lstHospital[0].lat, lstHospital[0].lon],
    [10.78280,106.70556],
    [lat, lon]
];
*/

var xecuuthuong2 = [
    [lstHospital2[0].lat, lstHospital2[0].lon],
    [10.73758,106.73041],
    [lat, lon]
];


var londonBrusselFrankfurtAmsterdamLondon = [
    [51.507222, -0.1275],
    [50.85, 4.35],
    [50.116667, 8.683333],
    [52.366667, 4.9],
    [51.507222, -0.1275]
];

var barcelonePerpignanPauBordeauxMarseilleMonaco = [
    [41.385064, 2.173403],
    [42.698611, 2.895556],
    [43.3017, -0.3686],
    [44.837912, -0.579541],
    [43.296346, 5.369889],
    [43.738418, 7.424616]
];


//map.fitBounds(mymap);

map.setView([10.75605,106.70292], 14);

var FireIcon = L.icon({
    iconUrl: 'images/fire-2-32_2.gif',
    iconSize: [36, 36], // size of the icon
});

var CCIcon = L.icon({
    iconUrl: 'images/Ol_icon_blue_example.png',
    iconSize: [32, 32], // size of the icon
});

var CTIcon = L.icon({
    iconUrl: 'images/Ol_icon_red_example.png',
    iconSize: [32, 32], // size of the icon
});

// var CHIcon = L.icon({
//     iconUrl: 'images/jeep.png',
//     iconSize: [32, 32], // size of the icon
// });

var marker = L.marker([lat, lon], { icon: FireIcon }).bindPopup('<p style="font-size:20px;">Hiện Trường: <span style="color:blue">Khu Công nghiệp Tân Thuận</span></p>').addTo(map);
marker.openPopup();
// marker.on('mouseover', function(e) {
//         //open popup;
//         var popup = L.popup()
//             .setLatLng(e.latlng)
//             .setContent('<p style="font-size:20px;">Hiện Trường: <span style="color:blue">Khu Công Nghiệp Phúc Khánh</span></p>')
//             .openOn(map);
//     });

if (cuuhoa == 1) {
    //========================================================================
    var marker1 = L.Marker.movingMarker(xethang1,
        [10000,10000,2000,10000,15000,3000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);

    marker1.loops = 0;
    marker1.bindPopup();
    marker1.on('mouseover', function(e) {
        //open popup;
        var popup = L.popup()
            .setLatLng(e.latlng)
            // .setContent('Xe thang 1 thuộc: ' + lstFirefighter[0].name + '<br> Còn <strong>8</strong> phút nữa hiện trường')
            .setContent('Xe thang 1 <br> Còn <strong>8</strong> phút nữa đến hiện trường')
            .openOn(map);
    });
    marker1.start();

    var marker2 = L.Marker.movingMarker(xethang2,
        [2000, 5000, 10000, 10000, 15000, 2000, 2000],
        { autostart: false, loop: true, icon: CCIcon }).addTo(map);

    marker2.loops = 0;
    marker2.bindPopup();
    marker2.on('mouseover', function(e) {
        var popup = L.popup()
            .setLatLng(e.latlng)
            .setContent('Xe thang 2 <br> Còn <strong>5</strong> phút nữa đến hiện trường')
            .openOn(map);
    });
    marker2.start();
}

if (cuuthuong == 1) {
    var markerCT2 = L.Marker.movingMarker(xecuuthuong2,
        [5000, 15000], { autostart: false, loop: true, icon: CTIcon }).addTo(map);

    markerCT2.loops = 0;
    markerCT2.bindPopup();
    markerCT2.on('mouseover', function(e) {
        //open popup;
        var popup = L.popup()
            .setLatLng(e.latlng)
            .setContent('Xe cứu thương 2 <br> Còn <strong>5</strong> phút nữa đến hiện trường')
            .openOn(map);
    });
    markerCT2.start();
}

var route1 = new L.Routing.control({
    waypoints: [
        L.latLng(lstFirefighter[0].lat, lstFirefighter[0].lon),
        L.latLng(lat, lon)
    ],
    addWaypoints: false,
    lineOptions: {
        styles: [{ color: "red", opacity: 1, weight: 5 }]
    },
    createMarker: function() { return null; },
    draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);

var route2 = new L.Routing.control({
    waypoints: [
        L.latLng(lstHospital[0].lat, lstHospital[0].lon),
        L.latLng(lat, lon)
    ],
    addWaypoints: false,
    lineOptions: {
        styles: [{ color: "blue", opacity: 1, weight: 5 }]
    },
    createMarker: function() { return null; },
    draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}