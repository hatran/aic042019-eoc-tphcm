﻿$(function () {
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'container1',
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 0,
                beta: 30,
                depth: 50,
                viewDistance: 25
            },
            //events: {
            //    load: function () {
            //        var count = 0;
            //        setInterval(function () {
            //            if (count == 0) {
            //                chart.series[0].setData([1, 20,15,45 ]);
            //                //chart.series[1].setData([10.57]);
            //                //chart.series[2].setData([7.23]);
            //                count = 1;
            //            }
            //            else {
            //                chart.series[0].setData([0, 0,0,0]);
            //                //chart.series[1].setData([0]);
            //                //chart.series[2].setData([0]);
            //                count = 0;
            //            }
            //        }, 1900);
            //    }
            //}
        },
        title: {
            text: 'BÁO CÁO 113',
            style: {
                color: '#000000',
                fontWeight: 'bold'
            }
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['biểu tình', 'trộm cắp', 'khiếu kiện', 'đánh nhau']
        },
        yAxis: {
            max:46,
            title: {
                text: 'Số vụ'
            }
        },
        colors: ['#9c27b0', '#ffc107', '#2196f3', '#00ffff'],

        plotOptions: {
            column: {
                depth: 40,
                colorByPoint: true
            }
        },
        series: [{
            showInLegend: false,
            data: [1, 20, 15, 45]
        },
        ],
        credits: {
            enabled: false
        },
        exporting: { enabled: false }
    });

    function showValues() {
        $('#alpha-value').html(chart.options.chart.options3d.alpha);
        $('#beta-value').html(chart.options.chart.options3d.beta);
        $('#depth-value').html(chart.options.chart.options3d.depth);
    }

    // Activate the sliders
    $('#sliders input').on('input change', function () {
        chart.options.chart.options3d[this.id] = parseFloat(this.value);
        showValues();
        chart.redraw(false);
    });

    showValues();
});